#!/usr/bin/env python
#
# season_math_test.py - convert from month to season and vice versa
#
# Copyright (C) 2015  Antonio Ospite <ao2@ao2.it>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.

# The code assumes meteorological seasons.
# See http://en.wikipedia.org/wiki/Season

seasons_norhtern = ["Winter", "Spring", "Summer", "Autumn"]
seasons_southern = ["Summer", "Autumn", "Winter", "Spring"]

# Default seasons order is the one for the Northern hemisphere
seasons = seasons_norhtern

months = ["January", "February", "March", "April", "May", "June", "July",
          "August", "September", "October", "November", "December"]


def season(month):
    if month < 0 or month > 11:
        raise ValueError("Invalid month %d" % month)

    # Rotate forward by one month because Winter starts in December,
    # and group by three months because each season is three months long.
    return ((month + 1) % 12) // 3


def first_month_of_season(season):
    if season < 0 or season > 3:
        raise ValueError("Invalid season %d" % season)

    # Expand by three because a new season starts every 3 months,
    # and rotate back by one month because December is the first
    # month of Winter
    return (season * 3 - 1) % 12


def test(function):
    try:
        eval(function)
    except Exception as e:
        print e.message


if __name__ == "__main__":
    test("season(-1)")
    test("season(12)")
    test("first_month_of_season(-1)")
    test("first_month_of_season(4)")

    print

    for i, m in enumerate(months):
        j = season(i)
        print "Month: %s. (%d) \tSeason: %s (%d)" % (m[0:3], i, seasons[j], j)

    print

    for i, s in enumerate(seasons):
        j = first_month_of_season(i)
        print "Season: %s (%d)\tFirst month: %s (%d)" % (s, i, months[j], j)
